using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : HashSet<TerrainFace> {
	
	public int maxStictchedVerts = -1;
	
	public EdgeSet GenerateEdgeSet() {
		EdgeSet edgeSet = new EdgeSet();
		foreach (TerrainFace terrainFace in this) {
			foreach (TerrainFace neighbor in terrainFace.neighbors) {
				if (this.Contains(neighbor))
						continue;
				Edge edge = new Edge(terrainFace, neighbor);
				edgeSet.Add(edge);
				}
			}
			return edgeSet;
	}

	public List<int> GetUniqueVerts() {
			List<int> verts = new List<int>();
			foreach (TerrainFace face in this) {
				foreach (int vert in face.vertIndexes) {
					if (!verts.Contains(vert))
						verts.Add(vert);
				}
			}
			return verts;
	}
}
