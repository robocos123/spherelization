﻿	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;


	public class Edge {
		
	public TerrainFace innerFace; 
	public TerrainFace outterFace;

	public List<int> outterVerts; 
	public List<int> innerVerts; 

	public Edge(TerrainFace innerFace, TerrainFace outterFace) {
			this.innerFace = innerFace;
			this.outterFace = outterFace;
			outterVerts = new List<int>(2);
			innerVerts = new List<int>(2);

			foreach (int vertex in innerFace.vertIndexes) {
				if (outterFace.vertIndexes.Contains(vertex))
					innerVerts.Add(vertex);
			}
			
			if(innerVerts[0] == innerFace.vertIndexes[0] && innerVerts[1] == innerFace.vertIndexes[2]) {
					int temp = innerVerts[0];
					innerVerts[0] = innerVerts[1];
					innerVerts[1] = temp;
			}
			outterVerts = new List<int>(innerVerts);
	}
}
