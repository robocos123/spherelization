	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class EdgeSet : HashSet<Edge> {
		
	public void UpdateEdges(List<int> oldVertices, List<int> newVertices) {
		foreach(Edge edge in this) {
			for(int i = 0; i < 2; i++) {
				edge.innerVerts[i] = newVertices[oldVertices.IndexOf(edge.outterVerts[i])];
			}
		}
	}

	public List<int> GetUniqueVerts() {
		List<int> vertices = new List<int>();
		foreach (Edge edge in this) {
			foreach (int vert in edge.outterVerts) {
				if (!vertices.Contains(vert))
					vertices.Add(vert);
				}
		}
		return vertices;
	}
}
