﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	
	// private bool locked = true;

	private MeshRenderer meshRenderer;
	private MeshFilter meshFilter;
	
	public new Camera camera;
	public float camDistance = 5f;
	
	public Transform pivot;
	
	public Rigidbody body;
	public float MASS = 100f;
	public float DENSITY = 2.7f;
	
	public float scale = 1/Planet.radius;
	public static Transform planet;
	
	public float speed, angularSpeed; 
	public float thrust, yawSpeed;
	private float distToPlanet;
	
	
	void Start() {
		
		meshRenderer = gameObject.AddComponent<MeshRenderer>();
		
		Mesh mesh = Resources.Load("Models/Plane", typeof(Mesh)) as Mesh;
		meshFilter = gameObject.AddComponent<MeshFilter>();
		meshFilter.mesh = mesh;
		
		Material[] mats = meshRenderer.sharedMaterials;
		mats[0] = Resources.Load("Materials/Texture", typeof(Material)) as Material;
		meshRenderer.sharedMaterial = mats[0];
		
		body = GetComponent<Rigidbody>();
		
		transform.localPosition = new Vector3(0, 1.5f * Planet.radius, 0);
		transform.localScale *= 100 * scale;
		pivot.transform.localPosition = new Vector3(0, 5f, -camDistance);
		
		//boost!
		thrust = Planet.MIN_ORBIT_SPEED / 2f;
		
		body.velocity = transform.forward * thrust;
		body.mass = MASS;
		
		transform.eulerAngles = new Vector3(0f, -90f, 0f);
	}
	
	void Update() {
		UpdateCam();
		UpdateInput();
	}
	
	void UpdateCam() {
		Quaternion initRot = pivot.rotation;
		pivot.LookAt(transform.position, transform.up);
		
		//store new rotation and lerp to it
		Quaternion newRot = pivot.rotation;
		pivot.rotation = initRot;
		pivot.rotation = Quaternion.Lerp(pivot.rotation, newRot, Time.deltaTime * angularSpeed);
		camera.transform.position = pivot.position;

	}
	
	void UpdateInput() {
		if(Input.GetKey(KeyCode.W)) {
			thrust += 0.01f;
		}
		if(Input.GetKey(KeyCode.S)) {
			thrust -= 0.01f;
		}
		
		// if(Input.GetKey(KeyCode.A)) {
		// 	yawSpeed += Planet.MIN_ORBIT_SPEED / 10f;
		// }
		
		// if(Input.GetKey(KeyCode.D)) {
		// 	yawSpeed -= Planet.MIN_ORBIT_SPEED / 10f;
		//  }
	}
	
	
	//Physics update
	void FixedUpdate() {
		Vector3 pos = transform.position - Planet.center;
		distToPlanet = 2 * Vector3.Magnitude(pos);
		speed = Vector3.Magnitude(body.velocity);

		if(distToPlanet <= 3.5f * Planet.radius) {
			float strength = Planet.G * Planet.MASS * MASS / (distToPlanet * distToPlanet);
			Vector3 force = strength * -pos.normalized;
			// body.AddForceAtPosition(acc, transform.position, ForceMode.Impulse);
			body.AddForce(force, ForceMode.VelocityChange);
			
			Vector3 drag = -transform.forward * MASS * 0.5f * Planet.DRAG_COEFFICIENT * speed * speed;
			body.AddForce(drag);
			
			angularSpeed = 400f * speed / distToPlanet;

			Vector3 pitchAxis = Vector3.Cross(Vector3.up, body.velocity.normalized);
			Quaternion pitchRot = Quaternion.AngleAxis(angularSpeed, pitchAxis);
			transform.localRotation = Quaternion.Slerp(transform.localRotation, pitchRot, angularSpeed * Time.deltaTime);
		}
		
		body.velocity = Vector3.Lerp(body.velocity, transform.right * yawSpeed + transform.forward * thrust, Time.deltaTime * speed);
	}
}
