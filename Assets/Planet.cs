﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour {
	
	public List<TerrainFace> faces;
	public List<Vector3> verts;
	
	public MeshFilter filter;
	public Material meshMat;
	public MeshCollider meshCollider;
	
	GameObject camObj;
	
	public int subdivisions = 4;
	
	public float minLandMassSize = 0.3f;
	public float maxLandMassSize = 0.6f;
	
	public float minMountainSize = 0.05f;
	public float maxMountainSize = 0.1f;
	public int numLandMasses, numMountains;
	
	//assume a large scale for mass of planet
	public static float MASS = 10000000000.0f;
	public static float G = 6.674f * Mathf.Pow(10, -11);
	public static float radius = 100f;
	public static Vector3 center = new Vector3(0, 50, 0);
	public static float MIN_ORBIT_SPEED = Mathf.Sqrt(G * MASS / radius); 
	public static float DRAG_COEFFICIENT = 0.01f;

	void Start() {
		numLandMasses = 5;
		numMountains = 5;
		Initialize();
		Subdivide(subdivisions);
		FindNeighbors();
		
		
		Color32 oceanBlue = new Color32(0, 80, 240, 0);
		Color32 grassGreen = new Color32(0, 240, 0, 0);
		Color32 dirtBrown = new Color32(180, 140, 20, 0);
		Color32 stoneGray = new Color32(169, 169, 169, 0);
		Color32 snowWhite = new Color32(255, 255, 255, 1);

		foreach(TerrainFace f in faces) {
			f.faceColor = oceanBlue;
		}
		
		Tile landTiles = new Tile();
		List<Vector3> usedPoints = new List<Vector3>();
		for(int i = 0; i < numLandMasses; i++) {
			float landMassSize = Random.Range(minLandMassSize, maxLandMassSize);
			Vector3 point = Random.onUnitSphere;
			if(usedPoints.Contains(point)) {
				point *= -1;
			}
			Tile tilesInRadius = GetFacesInRadius(point, landMassSize, faces);
			landTiles.UnionWith(tilesInRadius);
			usedPoints.Add(point);
		}
		
		foreach(TerrainFace face in landTiles) {
			face.faceColor = grassGreen;
		}

		//Connects top of extruded continents bordering ocean
		EdgeSet edges;
		// Color32 brownGreen = Color32.Lerp(dirtBrown, grassGreen, 0.3f);
		Tile connectorTiles = SmoothExtrude(landTiles, 0.05f, out edges);
		foreach(Edge edge in edges) {
			edge.outterFace.faceColor = dirtBrown;
			foreach(TerrainFace face in edge.outterFace.neighbors) {
				face.faceColor = dirtBrown;
			}
			
		}
		
		Tile mountains = new Tile();
		usedPoints.Clear();
		for(int i = 0; i < numMountains; i++) {
			float mountainSize = Random.Range(minMountainSize, maxMountainSize);
			//Create mountain tiles using existing land tiles
			Vector3 point = Random.onUnitSphere;
			usedPoints.Add(point);
			Tile mountainsFromLand = GetFacesInRadius(Random.onUnitSphere, mountainSize, landTiles);
			//mountainsFromLand = LevelTiles(mountainsFromLand, 0.75f);
			mountains.UnionWith(mountainsFromLand);
		}
		connectorTiles = SmoothExtrude(mountains, 0.2f, out edges);
		
		foreach(TerrainFace connector in connectorTiles) {
			connector.faceColor = stoneGray;
			foreach(TerrainFace face in connector.neighbors) {
				face.faceColor = stoneGray;
			}
		}
		
		
		//above land tiles
		foreach(Edge edge in edges) {
			edge.outterFace.faceColor = stoneGray;
			edge.innerFace.faceColor = stoneGray;
			foreach(TerrainFace face in edge.outterFace.neighbors) {
				face.faceColor = stoneGray;
			}
		}
		
		foreach(TerrainFace faceMountain in mountains) {
			faceMountain.faceColor = snowWhite;
		}
		
		BuildMesh();
		Player.planet = transform;
	}
	
	
	public void Initialize() {
			faces = new List<TerrainFace> ();
			verts = new List<Vector3> ();

			float t = (1.0f + Mathf.Sqrt(5.0f)) / 2.0f;

			verts.Add (new Vector3 (-1, t, 0).normalized);
			verts.Add (new Vector3 (1, t, 0).normalized);
			verts.Add (new Vector3 (-1, -t, 0).normalized);
			verts.Add (new Vector3 (1, -t, 0).normalized);
			verts.Add (new Vector3 (0, -1, t).normalized);
			verts.Add (new Vector3 (0, 1, t).normalized);
			verts.Add (new Vector3 (0, -1, -t).normalized);
			verts.Add (new Vector3 (0, 1, -t).normalized);
			verts.Add (new Vector3 (t, 0, -1).normalized);
			verts.Add (new Vector3 (t, 0, 1).normalized);
			verts.Add (new Vector3 (-t, 0, -1).normalized);
			verts.Add (new Vector3 (-t, 0, 1).normalized);

			// And here's the formula for the 20 sides,
			// referencing the 12 vertices we just created.
			faces.Add (new TerrainFace(0, 11, 5));
			faces.Add (new TerrainFace(0, 5, 1));
			faces.Add (new TerrainFace(0, 1, 7));
			faces.Add (new TerrainFace(0, 7, 10));
			faces.Add (new TerrainFace(0, 10, 11));
			faces.Add (new TerrainFace(1, 5, 9));
			faces.Add (new TerrainFace(5, 11, 4));
			faces.Add (new TerrainFace(11, 10, 2));
			faces.Add (new TerrainFace(10, 7, 6));
			faces.Add (new TerrainFace(7, 1, 8));
			faces.Add (new TerrainFace(3, 9, 4));
			faces.Add (new TerrainFace(3, 4, 2));
			faces.Add (new TerrainFace(3, 2, 6));
			faces.Add (new TerrainFace(3, 6, 8));
			faces.Add (new TerrainFace(3, 8, 9));
			faces.Add (new TerrainFace(4, 9, 5));
			faces.Add (new TerrainFace(2, 4, 11));
			faces.Add (new TerrainFace(6, 2, 10));
			faces.Add (new TerrainFace(8, 6, 7));
			faces.Add (new TerrainFace(9, 8, 1));
	}
	
	public void FindNeighbors() {
	foreach(TerrainFace f in faces) {
		foreach(TerrainFace f2 in faces) {
				if(f == f2) {
					continue;
				}
				if(f.isNeighbor(f2)) {
					f.neighbors.Add(f2);
				}
			}
		}
	}
	
	public List<int> CloneVerts(List<int> oldVerts) {
		List<int> clonedVerts = new List<int>();
		foreach(int oldVert in oldVerts) {
			Vector3 copyVert = verts[oldVert];
			clonedVerts.Add(verts.Count);
			verts.Add(copyVert);
		}
		return clonedVerts;
	}
	
	public void Subdivide(int level) {
		//need to be able to access midpoints to apply subdivisions
		//key takes pair: (key, vertexIndex)
		Dictionary<int, int> midpoints = new Dictionary<int, int>();
		
		for(int i = 0; i < level; i++) {
			//newFaces contains both original triangles and new ones formed by subdivision
			List<TerrainFace> newPolys = new List<TerrainFace>();
			foreach(TerrainFace poly in faces) {
				//a triangle with vertices, A B and C
				//indexes for each vertex
				int a = poly.vertIndexes[0];
				int b = poly.vertIndexes[1];
				int c = poly.vertIndexes[2];
				
				int ab = GetMidPoint(midpoints, a, b);
				int bc = GetMidPoint(midpoints, b, c);
				int ca = GetMidPoint(midpoints, c, a);

				newPolys.Add (new TerrainFace(a, ab, ca));
				newPolys.Add (new TerrainFace(b, bc, ab));
				newPolys.Add (new TerrainFace(c, ca, bc));
				newPolys.Add (new TerrainFace(ab, bc, ca));
				
			}
			
			faces = newPolys;
		}
	}
	
	public int GetMidPoint(Dictionary<int, int> midpoints, int indexA, int indexB) {
		//key for midpoint packs the smaller index with the larger index into a single integer
		int maxIndex = Mathf.Max(indexA, indexB);
		int minIndex = indexA + indexB - maxIndex;
		int key = (maxIndex << 16) + minIndex;
		int ret;
		
		//if midpoint for vertices already exists
		if(midpoints.TryGetValue(key, out ret)) {
			return ret;
		}
		
		//Store in key index
		Vector3 pointA = verts[indexA];
		Vector3 pointB = verts[indexB];
		Vector3 midPoint = Vector3.Lerp(pointA, pointB, 0.5f).normalized;
		ret = verts.Count;
		verts.Add(midPoint);
		midpoints.Add(key, ret);
		
		return ret;
		
	}
	
	public Tile PatchTerrain(Tile tiles, out EdgeSet tileEdges) {
		Tile patchedFaces = new Tile();
		
		tileEdges = tiles.GenerateEdgeSet();
		//Create a patch of triangles(group of tiles) between extruded triangles
		List<int> originalVerts = tileEdges.GetUniqueVerts();
		List<int> newVerts = CloneVerts(originalVerts);
		tileEdges.UpdateEdges(originalVerts, newVerts);
		
		foreach(Edge edge in tileEdges) {
			//insert new edge with vertices A' and B' between edge AB and connect new triangles
			//patch remaining area with new triangles A'->B'->A, B'->B->A
			TerrainFace patchFace1 = new TerrainFace(edge.outterVerts[0], edge.outterVerts[1], edge.innerVerts[0]);
			TerrainFace patchFace2 = new TerrainFace(edge.outterVerts[1], edge.innerVerts[1], edge.innerVerts[0]);
			edge.innerFace.ReplaceNeighbor(edge.outterFace, patchFace2);
			edge.outterFace.ReplaceNeighbor(edge.innerFace, patchFace1);
			faces.Add(patchFace1);
			faces.Add(patchFace2);
			
			patchedFaces.Add(patchFace1);
			patchedFaces.Add(patchFace2);
		}
		
		foreach(TerrainFace face in faces) {
			for (int i = 0; i < 3; i++) {
				int vertID = face.vertIndexes[i];
				if (!originalVerts.Contains(vertID))
						continue;
				int vertIndex = originalVerts.IndexOf(vertID);
				face.vertIndexes[i] = newVerts[vertIndex];
			}
		}
		return patchedFaces;
	}
	
	public Tile SmoothExtrude(Tile tile, float height, out EdgeSet edges) {
		Tile patchedFaces = PatchTerrain(tile, out edges);
		List<int> vertices = tile.GetUniqueVerts();
		foreach (int vert in vertices) {
			Vector3 v = verts[vert];
			v = v.normalized * (v.magnitude + height);
			verts[vert] = v;
		}
		return patchedFaces;
	}
	
	public Tile GetFacesInRadius(Vector3 center, float radius, IEnumerable<TerrainFace> facesInRadius) {
		Tile tiles = new Tile();
		foreach(TerrainFace face in facesInRadius) {
			foreach(int vertexIndex in face.vertIndexes) {
				float sphereDist = Vector3.Distance(center, verts[vertexIndex]);
				if(sphereDist <= radius) {
					//Add entire face if one of its vertices are within radius
					tiles.Add(face);
					break;
				}
			}
		}
		return tiles;
	}
	
	public Vector3 GetCenter(Tile region) {
		Vector3 centerPoint = Vector3.zero;
		int count = 0;
		foreach(TerrainFace face in region) {
			centerPoint += face.GetCentroid(verts);
			++count;
		}
		centerPoint /= count;
		return centerPoint;
	}

	public void BuildMesh() {
		MeshRenderer surfaceRenderer = gameObject.AddComponent<MeshRenderer>();
		meshMat = Resources.Load("Materials/VertexColor", typeof(Material)) as Material;
		surfaceRenderer.material = meshMat;
		
		meshCollider = gameObject.AddComponent<MeshCollider>();
		
		Mesh mesh = new Mesh();
		int vertCount = faces.Count * 3;
		int[] indicies = new int[vertCount];
		
		Vector3[] vertices = new Vector3[vertCount];
		Vector3[] normals = new Vector3[vertCount];
		Color32[] colors = new Color32[vertCount];
	
		for(int i = 0; i < faces.Count; i++) {
			TerrainFace face = faces[i];
			indicies[i * 3] = i * 3;
			indicies[i * 3 + 1] = i * 3 + 1;
			indicies[i * 3 + 2] = i * 3 + 2;
			
			vertices[i * 3] = verts[face.vertIndexes[0]];
			vertices[i * 3 + 1] = verts[face.vertIndexes[1]];
			vertices[i * 3 + 2] = verts[face.vertIndexes[2]];
			
			//Color32 faceColor = Color32.Lerp(green, brown, Random.Range(0, 1.0f));
			/*colors[i * 3] = faceColor;
			colors[i * 3 + 1] = faceColor;
			colors[i * 3 + 2] = faceColor;
			*/
			
			colors[i * 3] = face.faceColor;
			colors[i * 3 + 1] = face.faceColor;
			colors[i * 3 + 2] = face.faceColor;
			
			if(face.smoothNormals) {
				normals[i * 3] = vertices[i * 3].normalized;
				normals[i * 3] = vertices[i * 3 + 1].normalized;
				normals[i * 3 + 2] = vertices[i * 3 + 2].normalized;
				
			}else {
				Vector3 axisAB = verts[face.vertIndexes[1]] - verts[face.vertIndexes[0]];
				Vector3 axisAC = verts[face.vertIndexes[2]] - verts[face.vertIndexes[0]];
				Vector3 normal = Vector3.Cross(axisAB, axisAC).normalized;
				
				normals[i * 3] = normal;
				normals[i * 3 + 1] = normal;
				normals[i * 3 + 2] = normal;
			}
		}
		
		for(int i = 0; i < verts.Count; i++) {
			verts[i] += Planet.center;
		}
		
		mesh.vertices = vertices;
		mesh.normals = normals;
		mesh.colors32 = colors;
		mesh.SetTriangles(indicies, 0);
		
		filter = gameObject.AddComponent<MeshFilter>();
		filter.mesh = mesh;
		meshCollider.sharedMesh = mesh;
		
		
		transform.localScale *= radius;
	}
}
