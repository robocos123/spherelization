﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/VertexColor"
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "black" { }
		
		[PowerSlider(3.0)]
		_Outline("Outline width", Range(0.0, 0.5)) = 0.05
		_EdgeColor("Edge color", color) = (1.0, 1.0, 0.6, 1.0)
	}
	
	SubShader
	{	
		Pass
		{
			CGPROGRAM
			#include "UnityCG.cginc"
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom
			
			
			struct v2g {
				float4 color : COLOR;
				float3 normal : NORMAL;
				float4 worldPos : SV_POSITION;
			};
			
			struct g2f {
				float4 pos : SV_POSITION;
				float3 barycenter : TEXCOORD0;
				float4 color : COLOR;
			};
			
			v2g vert(appdata_full v) {
				v2g o;
				float3 pos = v.vertex;
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				o.normal = normalize(v.vertex);
				o.color = v.color;
				return o;
			}
			
			[maxvertexcount(3)]
			void geom(triangle v2g IN[3], inout TriangleStream<g2f> triStream) {
				//offset to shift barycenter by
				float3 diagRemove = float3(0.0, 0.0, 0.0);
				float AB = length(IN[0].worldPos - IN[1].worldPos);
				float BC = length(IN[1].worldPos - IN[2].worldPos);
				float CA = length(IN[2].worldPos - IN[0].worldPos);
				
				//stricly greater, as AB = BC = CA would be an equilateral triangle so no edge is a diagonal
				if(AB > BC && AB > CA) {
					diagRemove.y = 1.0;
				}else if(BC > AB && BC > CA) {
					diagRemove.x = 1.0;
				}else if(CA > AB && CA > BC) {
					diagRemove.z = 1.0;
				}
				
				g2f o;
				//hold onto triangle vertices with pos and barycenter data
				o.pos = mul(UNITY_MATRIX_VP, IN[0].worldPos);
				o.barycenter = float3(1.0, 0.0, 0.0) + diagRemove;
				o.color = IN[0].color;
				triStream.Append(o);
				
				o.pos = mul(UNITY_MATRIX_VP, IN[1].worldPos);
				o.barycenter = float3(0.0, 0.0, 1.0) + diagRemove;
				o.color = IN[1].color;
				triStream.Append(o);
				
				o.pos = mul(UNITY_MATRIX_VP, IN[2].worldPos);
				o.barycenter = float3(0.0, 1.0, 0.0) + diagRemove;
				o.color = IN[2].color;
				triStream.Append(o);
				
			}
			
			float _Outline;
			fixed4 _EdgeColor;
			
			fixed4 frag(g2f i) : SV_Target {
				if(!any(bool3(i.barycenter.x < _Outline, i.barycenter.y < _Outline, i.barycenter.z < _Outline))) {
					return i.color;
				}
				return _EdgeColor;
			}
			ENDCG
		}
	}
Fallback "Diffuse"
}