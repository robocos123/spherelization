﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainFace {

	public List<int> vertIndexes;
	public List<TerrainFace> neighbors;
	public Color32 faceColor;
	public bool smoothNormals;
	
	public TerrainFace(int vertIndexA, int vertIndexB, int vertIndexC) {
		vertIndexes = new List<int>() {vertIndexA, vertIndexB, vertIndexC};
		neighbors = new List<TerrainFace>();
		
		smoothNormals = true;
		faceColor = new Color32(255, 0, 255, 0);
	}
	
	public bool isNeighbor(TerrainFace other) {
		int vertNeighbors = 0;
		foreach(int vert in vertIndexes) {
			if(other.vertIndexes.Contains(vert)) {
				vertNeighbors++;
			}
		}
		return vertNeighbors == 2;
	}
	
	public void ReplaceNeighbor(TerrainFace oldFace, TerrainFace newFace) {
		for(int i = 0; i < neighbors.Count; i++) {
			if(neighbors[i] == oldFace) {
				neighbors[i] = newFace;
				return;
			}
		}
	}
	
	public Vector3 GetCentroid(List<Vector3> vertices) {
		Vector3 vertA = vertices[vertIndexes[0]];
		Vector3 vertB = vertices[vertIndexes[1]];
		Vector3 vertC = vertices[vertIndexes[2]];
		
		return (vertA + vertB + vertC)/3.0f;
	}
}
